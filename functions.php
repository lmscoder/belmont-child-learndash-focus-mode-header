<?php
/**
 * Enqueue scripts and styles.
 */
function belmont_child_scripts() {
	wp_dequeue_style( 'belmont-style' );

	wp_enqueue_style( 'belmont-parent-style', get_template_directory_uri() . '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'belmont_child_scripts' );

function belmont_insert_header_in_focus_mode() {
	get_template_part( 'partials/header-top' );
}
add_action( 'learndash-focus-template-start', 'belmont_insert_header_in_focus_mode' );

function belmont_focus_mode_css() { ?>
	<style>
		.ld-focus .header-top {
			display: none;
		}

		@media (min-width: 1001px) {
			.ld-focus .ld-focus-header {
				border-top: 1px solid #e2e7ed;
			}

			.ld-focus .header-top {
				display: block;
				position: fixed;
				top: 0;
				z-index: 100;
				width: 100%;
			}
		
			body.admin-bar .ld-focus .header-top {
				top: 32px;
			}

			.learndash-wrapper .ld-focus-header,
			.learndash-wrapper .ld-focus-sidebar {
				top: 82px;
			}

			body.admin-bar .learndash-wrapper .ld-focus-header,
			body.admin-bar .learndash-wrapper .ld-focus-sidebar {
				top: 114px;
			}

			.learndash-wrapper .ld-focus .ld-focus-main .ld-focus-content {
				padding-top: calc(5em + 82px);
			}

			body.admin-bar .learndash-wrapper .ld-focus .ld-focus-main .ld-focus-content {
				padding-top: calc(5em + 114px);
			}
		}
	</style>
<?php }
add_action( 'learndash-focus-head', 'belmont_focus_mode_css' );

