<div class="header-top">
	<nav id="site-navigation" class="main-navigation" role="navigation">
		<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Menu', 'belmont' ); ?></button>
		<?php wp_nav_menu( array( 'theme_location' => 'menu-1', 'menu_id' => 'primary-menu' ) ); ?>
	</nav><!-- #site-navigation -->

	<?php if ( get_theme_mod( 'belmont_header_text' ) || is_customize_preview() ) : ?>
		<div class="header-top-text"><?php echo wptexturize( esc_html( get_theme_mod( 'belmont_header_text' ) ) ); ?></div>
	<?php endif; ?>
</div><!-- .header-top -->
